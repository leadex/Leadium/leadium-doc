= Leadium Library

== Description

Leadium - implementation library for web UI / rest api / jdbc test automation in https://kotlinlang.org/[Kotlin].

=== Key features

* Structured test case scenarios (POM)

[source]
----
...
LoginPage() - {
    setLogin(login)
    setPassword(password)
    signIn()
}
MainPage() - {
    checkTitle()
    next()
    Dialog() - {
        checkTitle()
        accept()
    }
    Database() - {
        ...
    }
    ...
}
...
----
'''
* Display name generator by class reference

[source]
----
@DisplayNameGeneration(DisplayNameGenerator::class)
class ExampleSuite : TestSuite() {

    @Test
    @TestCaseDisplayName(example.cases.ExampleTest::class)
    fun exampleTest() {
        ...
    }
}
----
display name: *Example Test | cases | example*

'''

* https://junit.org/junit5/[JUnit5] extension for running https://selenium.dev/[Selenium] tests in parallel mode in https://www.docker.com/[Docker] containers via https://aerokube.com/selenoid/latest/[Selenoid].

[source]
----
if (System.getProperty("parallel", "true").toBoolean())
    Configuration.browser = Selenoid::class.java.canonicalName
----


== Environment setting

=== Intellij IDEA

* Install https://www.jetbrains.com/idea/download[Intellij IDEA]
* Download project with examples from https://gitlab.com/leadex/Leadium/leadium-examples[leadium-examples].
* Open the downloaded project in Intellij IDEA and wait when indexing to complete.

== How to start

=== Project creation

A step-by-step description of https://www.jetbrains.com/help/idea/getting-started-with-gradle.html[creating a project in Idea].

=== Module creation

==== In the root of the project
* Specify *sourceSets* in https://gitlab.com/leadex/Leadium/leadium-examples/blob/master/build.gradle[build.gradle] .

[source]
----
subprojects {

    sourceSets.getByName("test") {
        java.srcDir("src")
    }

    sourceSets.getByName("test") {
        resources.srcDir("res")
    }
}
----

* In the *source* directory create directory with the new module.

==== Inside the new module directory
* create https://gitlab.com/leadex/Leadium/leadium-examples/blob/master/src/apps/example/settings.gradle[settings.gradle].

add line with the module name.

[source, groovy]
----
rootProject.name = 'example'
----

* create the *src* directory (Test Sources Root).
* create the *res* directory (Test Resources Root).
* Add the new module to the parent project https://gitlab.com/leadex/Leadium/leadium-examples/blob/master/settings.gradle[settings.gradle].

[source, groovy]
----
rootProject.name = 'leadium-examples'

include 'example'
project(':example').projectDir = file('src/apps/example')
----

Then click *Reimport All Gradle Projects* in the Gradle tab.

=== Project structure

Example:

[source]
----
.
├── build.gradle
├── dependencies.gradle
├── libs
│   ├── directorywalker.jar
│   ├── leadium-core.jar
│   ├── leadium-core-sources.jar
│   ├── leadium-junit5.jar
│   ├── leadium-junit5-sources.jar
│   ├── leadium-report-allure.jar
│   ├── leadium-report-allure-sources.jar
│   ├── leadium-report.jar
│   ├── leadium-report-sources.jar
│   ├── leadium-ui.jar
│   └── leadium-ui-sources.jar
├── README.md
├── res
│   ├── allure.properties
│   ├── auth.property
│   └── categories.json
├── settings.gradle
└── src
    └── apps
        └── example
            ├── res
            │   ├── allure.properties
            │   ├── dev
            │   │   └── example.property
            │   └── video.property
            ├── settings.gradle
            └── src
                ├── allure
                │   └── ReportAdapter.kt
                └── example
                    ├── alias.kt
                    ├── AOP.kt
                    ├── cases
                    │   ├── ExampleTest.kt
                    │   └── syntax
                    │       └── TestDataSyntax.kt
                    ├── Properties.kt
                    ├── steps
                    │   └── pages
                    │       ├── DuckDuckGo.kt
                    │       └── Wikipedia.kt
                    └── suite
                        ├── ExampleSuite.kt
                        └── TestSuite.kt


----

'''
=== Test Creation

==== Basic concepts

An HTML element is a type of HTML document component, one of several types of HTML nodes.
HTML document is composed of a tree of simple HTML nodes, such as text nodes, and HTML elements, which add semantics and formatting to parts of document.
Each element can have HTML attributes specified. Elements can also have content, including other elements and text.

https://github.com/selenide-examples/kotlin/blob/588132ba59b36e3c79e7f833f4a9f6f9739dcb70/src/test/kotlin/GoogleTest.kt#L31[Selenide examples]

All actions with elements of a web page must be described in an own class depends on this page.
[source]
----
package steps.pages

import com.codeborne.selenide.Condition.*
import org.leadium.core.*
import org.leadium.data.*
import org.openqa.selenium.*

class DuckDuckGo : AbstractStep, XpathPresetSyntax { <1>

    @Step <2>
    fun setSearchBox(searchRequest: String) {
        element(By.xpath("//input[@id='search_form_input_homepage']"))
            .setValue(searchRequest)
    }

    @Step
    fun enter() {
        element(By.xpath("//input[@id='search_form_input_homepage']"))
            .sendKeys(Keys.ENTER)
    }

    @Step
    fun checkResult(expectedResult: String) {
        element(By.xpath(xpathByText(expectedResult, withText = true)))
            .should(be(visible))
    }

    @Step
    fun clickResult() {
        element(By.xpath("//*[text()='More at Wikipedia ']"))
            .click()
    }
}
----
<1> Class DuckDuckGo extends AbstractStep (and XpathPresetSyntax optional)
<2> Step Annotation for AOP StepProvider

==== Test case.

Right-click on cases-directory and select *New -> Kotlin File / Class*, enter the name of the test class, then Ok.

In the created file:

* Declare the class with the test name
* Implement the TestCase interface and redefine the *begin* method.

[source]
----
import com.codeborne.selenide.Condition.*
import example.TestDataMap
import example.cases.syntax.TestDataSyntax
import example.steps.pages.DuckDuckGo
import example.steps.pages.Wikipedia
import org.leadium.core.TestCase
import org.leadium.core.minus
import org.leadium.report.allure.description.Description

@Description(
"""
Example Test Description
"""
)
class ExampleTest(override val testData: TestDataMap) : TestCase, TestDataSyntax { <1>

    override fun begin() { <2>

        DuckDuckGo() - { <3>
            setSearchBox(searchRequest())
            enter()
            checkResult(expectedResult())
            clickResult()
        }
        Wikipedia() - {
            checkResult(expectedResult())
            checkLogoTitle()
            checkSearchInput()
            mathematics()
            mainPage()
            checkLogoCondition(be(not(focused)))
        }
    }
}
----
<1> Specify the name of the class and implement the *TestCase* interface.
<2> Override the *begin* method
<3> The operator *-* is required to call class methods (i.e. https://gitlab.com/leadex/Leadium/leadium-examples/blob/master/src/apps/example/src/example/steps/pages/DuckDuckGo.kt[DuckDuckGo] or https://gitlab.com/leadex/Leadium/leadium-examples/blob/master/src/apps/example/src/example/steps/pages/Wikipedia.kt[Wikipedia]) and display their title in the report.

Now we need to create ExampleSuite class in suite directory and then add created test-case there.

[source]
----
...
@Epic("EXAMPLE EPIC") <1>
@DisplayName("Example Suite | positive") <2>
@DisplayNameGeneration(DisplayNameGenerator::class) <3>
class ExampleSuite : TestSuite() { <4>

    @Test <5>
    @Feature("POSITIVE") <6>
    @Story("EXAMPLE STORY")
    @TestCaseDisplayName(ExampleTest::class) <7>
    @TestDataResolver.Settings(TestDataType.PROPERTY, resourcesPath, ["dev/example.property"])
    fun wikiTest(@Inject testData: PropertyTestData) {
        ExampleTest(testData.map).begin() <8>
    }
...
}

----
<1> Specify the @Epic annotation to indicate the epic group in the report.
<2> Specify the @DisplayName annotation to display the test suite name in the report.
<3> Specify the @DisplayNameGeneration annotation for auto-formatting of names in the report tests.
<4> The ExampleSuite class extends the class https://gitlab.com/leadex/Leadium/leadium-examples/blob/master/src/apps/example/src/example/suite/TestSuite.kt[TestSuite].
<5> Specify the annotation @Test, which means that this method is a test.
<6> Specify the annotations @Feature and @Story, defining subgroups for the report.
<7> Pass the test case class to the @TestCaseDisplayName annotation to display the name in the report. It is also possible to use the annotation https://docs.qameta.io/allure/#_displayname[@DisplayName] instead of the annotation @TestCaseDisplayName.
<8> Call the test-case begin method.

== Test running

To run the test, click *run* in Idea on the left of the test declaration.

image::pics/2.gif[]

=== Test running by console

To start the test suite, execute the command from the project directory:

[source,shell]
----
./gradlew :example:test --tests example.suite.ExampleSuite -i
----

=== Parallel tests execution by console

==== Docker installation

https://www.docker.com/get-started[Docker] need for Selenoid execution.

==== Selenoid Installation

Selenoid is Selenium Hub successor running browsers within Docker containers.
Scalable, immutable, self hosted Selenium-Grid is on every platform with single binary.

It is required to download binary of https://github.com/aerokube/selenoid/releases[Selenoid]
and https://github.com/aerokube/selenoid-ui/releases[Selenoid-ui].

For working Selenoid, we need to download docker-images for browsers and for video-recorder:

[source,shell]
----
docker pull selenoid/video-recorder
docker pull selenoid/vnc:chrome_65.0
----

Create docker network:

[source,shell]
----
docker network create \
  --driver=bridge \
  --subnet=172.29.0.0/16 \
  --ip-range=172.29.5.0/24 \
  --gateway=172.29.5.254 \
  bridge1
----

Before running the tests, we need to execute *selenoid* and *selenoid-ui*:

[source,shell]
----
./selenoid -container-network bridge1 -timeout 3m0s -limit 4
./selenoid-ui -listen :8090
----

*Selenoid* system properties initialization in build.gradle:

[source]
----
   tasks.withType(Test) {
        useJUnitPlatform()

        def defaultHost = "dev"

        systemProperty "host", System.getProperty("host", defaultHost) <1>
        systemProperty "envTestDataDir", System.getProperty("envTestDataDir", defaultHost)

        systemProperty "parallel", System.getProperty("parallel", "false") <2>

        systemProperty "leadium.selenoid.browserName", "chrome" <3>
        systemProperty "leadium.selenoid.enableVNC", true <4>
        systemProperty "leadium.selenoid.enableVideo", true <5>
        systemProperty "leadium.selenoid.host", "http://jenkins-01.example.com:4444" <6>
        systemProperty "leadium.selenoid.localhost", "http://localhost:4444" <7>
        systemProperty "leadium.selenoid.dimensionWidth", 1280 <8>
        systemProperty "leadium.selenoid.dimensionHeight", 1024 <9>
    }
----
<1> Add the *-Dhost* flag to gradle tasks of type Test. In this flag we specify the host where testing will be performed.
<2> Add the *-Dparallel* flag to enable parallel mode when *=true*.
<3> Browser name.
<4> Enable https://aerokube.com/selenoid/latest/#_live_browser_screen_enablevnc[VNC].
<5> Enable video recording in containers.
<6> The host where published the video files list with the record of passing tests from containers.
<7> The local host where published the video files list with recording tests from the containers.
<8> Video resolution, width.
<9> Video resolution, height.

==== Parallel running

For parallel running execute the command in terminal (one or more):
[source,shell]
----
./gradlew -Dparallel=true :example:test --tests example.suite.ExampleSuite1 -i
----

* The -Dparallel flag is used to start each process in a separate container.
* The --tests flag is used to specify the current test suite reference (--tests example.suite.ExampleSuite).

=== Generating Allure Report

* Download the latest version of https://github.com/allure-framework/allure2/releases[Allure]

Execute this command:

[source,shell]
----
./allure serve /path/to/project_dir/test/apps/module_name/build/allure-results
----

You can see the report example below:

image::pics/3.gif[]

On the page https://docs.qameta.io/allure/#_overview_page[Overview] we see the main data of the finished tests.
More information on the categories can be seen at https://docs.qameta.io/allure/#_categories[Categories].
By clicking on a specific test case, opens the https://docs.qameta.io/allure/#_test_case_page[Test Case page].
It displays result data of the selected test case:

* Name which is generated *Display Name Generator* from the specified class reference passed to the annotation *@TestCaseDisplayName*.
* Attached files during the test.

== Links

https://selenide.org/javadoc/current/com/codeborne/selenide/Configuration.html[com.codeborne.selenide.Configuration]

https://github.com/SergeyPirogov/video-recorder-java#configuration[Video Recorder Java]


== Documentation

* https://leadex.gitlab.io/Leadium/leadium-core/[leadium-core]
* https://leadex.gitlab.io/Leadium/leadium-appian/[leadium-appian]
